#!/usr/bin/env/python
# -*- coding: utf-8 -*-

from arncore import Pane, TextBox, ImageBox, SequencePane
#from surface import Surface, LCD_display
import arncore.surface
import time, cProfile, sys

### TEST FUNCTIONS ###
        
def test_text():
    # text = "11223344556677889900112233445566778899001122334455667788990011"*5
    text = "1122334455667788990011223344556677889900112233445566778899"
    # text = "12345678901234567890123456789012345678901234567890123456789012"*10
    # text = "AB"*310
    pane = Pane()
    for i in range(10):
        box = TextBox(text = text, roll = 1, alignment = "left", position = (0,i),  size=(62,1),fill_char = " ")
        pane.add_pane(box)
    
    for i in range(200):
        pane.draw()
        pane.render()

def test_image_flash():
    img_black = Image_box(file = "all_black.bmp")
    img_white = Image_box(file = "all_white.bmp")
    img_black.draw()
    img_white.draw()
    for _ in range(100):
        img_black.render()
        img_white.render()
        
def test_image():
    # img = Image_box(file = "all_white.bmp")
    # img = Image_box(file = "all_black.bmp")
    img = ImageBox(file = "img/winged_hull.png")
    img.draw()
    img.render()
    
def test_combo():
    pane = Pane()
    box = TextBox(text = "Välkomna till Flygsektionen!", roll = 1, alignment = "center", size=(62,1))
    img = ImageBox(file = "img/winged_hull.png", position = (7, 1), size = (45,8), offset=(2,12))
    pane.add_pane(box)
    pane.add_pane(img)
    for _ in range(20):
        pane.draw()
        pane.render()    
   
def test_sequence():
    pane = Pane()
    sequence = SequencePane()
    box = TextBox(text = "Välkomna till Flygsektionen!", roll = 1, alignment = "center", size=(62,1))
    img = ImageBox(file = "img/winged_hull.png", position = (7, 1), size = (45,8), offset=(2,12))
    sequence.add_pane(box, 20)
    sequence.add_pane(img, 20)
    pane.add_pane(sequence)
    for _ in range(200):
        pane.draw()
        pane.render()
   
def test_maping_column():
    """
    draws columns of 8 pixels on row 6, 7, 8 and 9
    The first column should be lit on segmnet 1 in each row
    the second on segment 2 etc
    Row 6 should display columns on the upper part of LCD 1
    Row 7 should display columns on the upper part of LCD 2
    Row 8 should display columns on the lower part of LCD 1
    Row 9 should display columns on the lower part of LCD 2
    """
    pane = Pane()
    for seg_col in range(8):
        segment = surface.LCD_display()
        for row in range(8):
            segment.pixels[row*8 + seg_col] = 1
        pane.set_segment(5, seg_col*2, segment)
        pane.set_segment(6, seg_col*2+1, segment)
    for seg_col in range(8):
        segment = surface.LCD_display()
        for row in range(8):
            segment.pixels[8*8 + row*8 + seg_col] = 1
        pane.set_segment(7, seg_col*2, segment)
        pane.set_segment(8, seg_col*2+1, segment)
    pane.render()    

def test_maping_single_row():
    """
    draws rows of 8 pixels on row 6, 7, 8 and 9
    The first row should be lit on segment 1 in each row
    the second on segment 2 etc
    Row 6 should display rows on the upper part of LCD 1
    Row 7 should display rows on the upper part of LCD 2
    Row 8 should display rows on the lower part of LCD 1
    Row 9 should display rows on the lower part of LCD 2
    """
    pane = Pane()
    for seg_index in range(8):
        segment = surface.LCD_display()
        for pixel in range(8):
            segment.pixels[seg_index*8 + pixel] = 1
        pane.set_segment(5, seg_index*2, segment)
        pane.set_segment(6, seg_index*2+1, segment)
    for seg_index in range(8):
        segment = surface.LCD_display()
        for pixel in range(8):
            segment.pixels[64 + seg_index*8 + pixel] = 1
        pane.set_segment(7, seg_index*2, segment)
        pane.set_segment(8, seg_index*2+1, segment)
    pane.render()
    
def fill_first():
    pane = Pane()
    segment = surface.LCD_display()
    for pixel_index in range(16*8):
        segment.pixels[pixel_index] = 1
    for seg_index in range(31): 
        pane.set_segment(7, seg_index*2, segment)
        pane.set_segment(8, seg_index*2+1, segment)
    pane.render()    
    
def main():
    # test_image()
    # test_image_flash()
    # test_text()
    # test_maping_column()
    # test_maping_single_column()
    # test_maping_row()
    # test_maping_single_row()
    # fill_first()
    # test_combo()
    test_sequence()
    # surface.Surface.render_thread.running = False

if __name__ == '__main__': main() #cProfile.run("main()")
