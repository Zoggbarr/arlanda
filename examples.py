#!/usr/bin/env/python

from arncore import Pane, TextBox, ImageBox, SequencePane
import time, cProfile, sys

def wupp():
    pane = Pane()
    pane2 = Pane()
    box = TextBox(text = "Välkomna till Flygsektionen!", roll = 1, alignment = "center", size=(62,1))
    box2 = TextBox(text = "ÖPH ser dig!", roll = 1, alignment = "center", size=(62,1))
    img = ImageBox(file = "img/winged_hull.png", position = (7, 1), size = (45,8), offset=(2,12))
    img2 = ImageBox(file = "img/winged_hull.png", position = (7, 1), size = (45,8), offset=(2,12))
    pane.add_pane(box)
    pane.add_pane(img)
    pane2.add_pane(img2)
    pane2.add_pane(box2)
    try:
        for _ in range(1):
        # while True:
            for _ in range(19*62):
                pane.draw()
                pane.render()
            for _ in range(1*62):
                pane2.draw()
                pane2.render()
    except KeyboardInterrupt:
        sys.exit()

def sequence_ex():
    pane = Pane()
    box = TextBox(text = "Välkomna till Flygsektionen!", roll = 0.5, alignment = "center", size=(62,1))
    box2 = TextBox(text = "ÖPH ser dig!", roll = 0.5, alignment = "center", size=(62,1))
    img_box = ImageBox(file = "img/winged_hull.png", position = (7, 1), size = (45,8), offset=(2,12))
    seq = SequencePane()
    pane.add_pane(img_box)
    seq.add_pane(box, 30)
    seq.add_pane(box2, 5)
    pane.add_pane(seq)
    pane.run()
    
    # pane = Pane()
    # box = TextBox(text = "Wrooooooooooooom!", roll = 1, size=(62,1))
    # img = ImageBox(file = "img/winged_hull.png", position = (7, 1), size = (45,8), offset=(2,12))
    # img2 = ImageBox(file = "wupp.bmp", position = (7, 1), size = (45,8), offset=(2,10))
    # seq = SequencePane()
    # pane.add_pane(box)
    # seq.add_pane(img, 30)
    # seq.add_pane(img2, 5)
    # pane.add_pane(seq)
    # pane.run()
        
def image():
    pane = Pane()
    img = ImageBox(file = "img/winged_hull.png", position = (7, 1), size = (45,8))
    pane.add_pane(img)
    pane.draw()
    pane.render()

def image_offset():
    pane = Pane()
    img = ImageBox(file = "img/winged_hull.png", position = (7, 1), size = (45,8), offset=(2,12))
    pane.add_pane(img)
    pane.draw()
    pane.render()
    
def text(): 
    """Displays text with different alignment"""
    pane = Pane()
    text_left = TextBox(text = "Hello left!", 
                         size=(62,1), 
                         position = (0, 1), 
                         alignment = "left")
    text_center = TextBox(text = "Hello center!", 
                           size=(62,1), 
                           position = (0, 2), 
                           alignment = "center")
    text_right = TextBox(text = "Hello right!", 
                          size=(62,1), 
                          position = (0, 4), 
                          alignment = "right")
    pane.add_pane(text_left)
    pane.add_pane(text_center)
    pane.add_pane(text_right)
    pane.draw()
    pane.render()
    
def text_roll(): 
    """Displays text"""
    pane = Pane()
    text_left = TextBox(text = "Hello left!", 
                         size=(62,1), 
                         position = (0, 1), 
                         alignment = "left",
                         roll=0.5)
    text_center = TextBox(text = "Hello center!", 
                           size=(62,1), 
                           position = (0, 2), 
                           alignment = "center")
    text_right = TextBox(text = "Hello right!", 
                          size=(62,1), 
                          position = (0, 3), 
                          alignment = "right",
                          roll=-0.5)
    pane.add_pane(text_left)
    pane.add_pane(text_center)
    pane.add_pane(text_right)
    pane.draw()
    pane.render()

def panes():
    pane = Pane()
    box = TextBox(text = "Välkomna till Flygsektionen!", 
                   alignment = "center", 
                   position = (0, 8),
                   size=(62,1))
    img = ImageBox(file = "img/winged_hull.png", 
                    position = (7, 0), 
                    size = (45,8), 
                    offset=(2,12))
    pane.add_pane(box)
    pane.add_pane(img)
    pane.draw()
    pane.render()
    
def main():
    # text()
    # image()
    # image_offset()
    # panes()
    # wupp()
    sequence_ex()
    
if __name__ == '__main__': main()
