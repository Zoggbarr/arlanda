# Screen Server readme

## Connecting to the screen server (RaspberryPi)
Open [https://gitforwindows.org/](Git Bash) and run `ssh pi@130.229.153.55`. Sign in with `SkFkLaPa5Pl`

### Running
1. Start screen by typing `screen`
2. Go to `tavla.git/tavla/ARN`
3. Start the python file with `sudo python main.py`
4. Press `Ctrl + A` then `D` to detach the screen session
5. Exit environment by running `exit`

### Stoping
1. Type `screen -ls` to view all screen sessions
2. Type `screen -D -r *session name*` to attach to the session running the process
3. Press Ctrl + C to stop the process

Kill a screen session with
`screen -X -S "sessionname" quit`

## Required libraries
..* Pillow
..* spidev
..* freetype

compile c-library with
`gcc -shared -o output_map.so output.c`


### Module explanation
#### examples.py
Runable file with a few working display examples. Some requires images.

#### fonts.py
Handles letters and fonts

#### output.c 
Contains functions to map the pixels from a sane maping to the order that the board wants them in. 

#### pane.py
Contains the `Pane` class, which inherits from `Surface`.
Also contains the following subclasses

    | Class | Description |
    | --- | --- |
    |`Leaf_pane` | Panes that renders things and are not just containers for other panes |
    | `Text_box` | A pane containing text |
    | `Image_box` | You get the picture |
    | `Sequence` | A pane that switches content |

#### py_output.py
Low level functions for comunicating with the board through the SPI buss.
Not too complicated but in general should not be touched unless for very good reasons.

#### surface.py
The base class for rendering things to the board. Contains multithreaded rendering stuff that should not be looked at for extended amounts of time. 

#### tests.py
Runable. Contains some basic tests of functionality.

#### utility_beer.py
Contains a function for displaying a list of beers and prices on the board. Work in progress. 