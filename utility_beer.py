#!/usr/bin/env/python
# -*- coding: utf-8 -*-

import pane
import time

COL_WIDTH = 31

def beer_list(beer):
    # categories = beer.split("CATEGORY")
    
    beers = beer.split('\n')
    beers = [b.rsplit(None, 1) if b else None for b in beers]
    print(beers)
    panes = []
    i = 0
    for beer in beers:
        if beer and len(beer) == 2:
            print(i)
            print(beer)
            if not i%18: panes.append(pane.Pane())
            row = i%18//2
            col = i%2 * COL_WIDTH
            print("row", row, "col", col)
            beer_name = pane.Text_box(text = beer[0], size=(25, 1), position = (col, row))
            price = pane.Text_box(text = beer[1], size=(5, 1), position=(col+25, row), alignment="right")
            panes[-1].add_pane(beer_name)
            panes[-1].add_pane(price)
            i +=1
    for i in range(10):
        for p in panes:
            p.draw()
            p.render()
            time.sleep(5)
    # for b in beers: print b

def main():
    beer_list(
"""TT 20
Äppelöl 30
God öl med långt namn 50
Mer öl 34
Ännu en öl 10
Äcklig öl 40
Cider 20
Två akrobatiskka dvärgar 700
En kaffe 25
Två Kaffear 35
Wupp 20
test 45
TT 20
Äppelöl 30
God öl med långt namn 50
Mer öl 34
Ännu en öl 10
Äcklig öl 40
Cider 20
Två akrobatiskka dvärgar 700
En kaffe 25
Två Kaffear 35
Wupp 20
test 45 
""")

if __name__ == '__main__': main()
        
    