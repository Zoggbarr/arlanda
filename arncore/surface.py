#!/usr/bin/env/python
# -*- coding: utf-8 -*-
"""
surface.py 

"""

# IMPORTS
from arncore import py_output
py_output.CLOSE_OUTPUT_AT_EXIT = False
# py_output.NO_OUTPUT = True
import ctypes
import copy
import time
import os
# import array
from array import array
import queue, atexit, multiprocessing
import cProfile

# CONSTANTS
PIXELS_HEIGHT = 16*10
PIXELS_WIDTH = 8*62
SEGMENTS_HEIGHT = 10
SEGMENTS_WIDTH = 62
LCD_HEIGHT = 16
LCD_WIDTH = 8

# CONFIG
DRAW_BUFFER = 4
DEBUG = 3

c_funcs = ctypes.CDLL('./arncore/output_map.so')

class Render_process(multiprocessing.Process):
    """ This is what runs in the thread that sends data to the board """
    def __init__(self, render_queue):
        multiprocessing.Process.__init__(self)
        self.daemon = True
        self.queue = render_queue
        self.rendercount = 0
    
    def run(self):
        running = True
        while running or not self.queue.empty():
            try:
                if DEBUG > 1: start_wait = time.time()
                try:
                    to_draw = self.queue.get(timeout=0.2)
                except queue.Empty:
                    print("Render queue empty!")
                    if not running: 
                        print("and not running, closing render process")
                        return
                    continue
                if not to_draw: 
                    print("Posion pill recieved, Shutting down render process")
                    running = False
                    return
                    #return  # Shutdown process if empty data is sent
                for row in range(len(to_draw)):
                    py_output.write_row(row, to_draw[row])
                py_output.latch()
                if DEBUG > 1: 
                    draw_finished = time.time()
                    print("frame:,", self.rendercount, "at time: ", time.time(), "\t conv delay:", draw_finished-start_wait)
                self.rendercount += 1
            except KeyboardInterrupt:
                print("Keyboard interrupt in render queue, preparing for shuttdown when queue is empty...")
                running = False

render_queue = multiprocessing.Queue(DRAW_BUFFER)
render_process = Render_process(render_queue)
render_process.start()
            
class Convert_process(multiprocessing.Process):
    """
    Process for converting the python representation of segments into something 
    that  the board understands.
    """
    def __init__(self, segments, noise = False, invert = False):
        multiprocessing.Process.__init__(self)
        self.daemon = True
        self.segments = segments
        self.noise = noise
        self.invert = invert
    
    def run(self):
        try:
            to_send = []
            for row in self.segments: 
                sendable = py_output.Row_bitmap()
                len_row = len(row)         
                for lcd_index in range(0, len_row, 2):
                    c_funcs.bmp2segmentOffset(row[lcd_index].pixels, row[lcd_index+1].pixels, int((len_row-lcd_index)/2 - 1), sendable)
                if not self.invert: #  We want 1 = white and 0 = black, and the board works the opposite way 
                    c_funcs.invert_row(sendable, py_output.bytes_per_segment*py_output.segments_per_row)
                to_send.append(tuple(sendable))
            if DEBUG > 1: print("Sending to queue")
            try: 
                render_queue.put(to_send, timeout=1.0)
            except queue.Full:
                print("Dropped frame")
                return
            if DEBUG > 1: print("Done Sending")
        except KeyboardInterrupt:
            if DEBUG > 1: print("convert process interrupted")
            return
            

class Surface(object):
    
    convert_processeses = []
    convert_queue = list()
    
    def __init__(self):
        self.segments = []  # Segments 
        self.writable = []  # Keeps track on if a segments pixels can be manipulated directly
        self.width = SEGMENTS_WIDTH
        self.height = SEGMENTS_HEIGHT
        for _ in range(self.height):
            self.segments.append( [LCD_display() for j in range(self.width)] )
            self.writable.append([True]*self.width)
        
    def put_pixel(self, row, col, color):
        segment_row = row // SEGMENTS_HEIGHT
        segment_col = col // SEGMENTS_WIDTH
        if not self.writable[segment_row][segment_col]:
            new_display = LCD_display(self.get_segment(segment_row, segment_col))
            self.set_segment(segment_row, segment_col, new_display)
            self.writable[segment_row][segment_col] = True
        pixel_row = row % SEGMENTS_HEIGHT
        pixel_col = col % SEGMENTS_WIDTH
        segment = self.get_segment(segment_row, segment_col)
        segment[pixel_row*8 + pixel_col] = color
    
    def single_render(self, invert = False, noise = False):
        rows = []
        for row_index in range(SEGMENTS_HEIGHT):
            if DEBUG > 1: print("rendering row ", row_index) 
            row = self.segments[row_index]       
            sendable = py_output.Row_bitmap()          
            for lcd_index in range(0, len(row), 2):
                c_funcs.bmp2segmentOffset(row[lcd_index].pixels, row[lcd_index+1].pixels, int((len(row)-lcd_index)/2 - 1), sendable)
            rows.append(tuple(sendable))
        render_queue.put(rows)
    
    def render(self, invert = False, noise = False, single =  False):
        if single:
            self.single_render()
            return
        processes = Surface.convert_processeses
        # Check if there are already enough processes runing
        while len(processes) >= DRAW_BUFFER:
            processes = [p for p in Surface.convert_processeses if p.is_alive()]
        # Create and append the new process
        processes.append(Convert_process(self.segments, noise, invert))
        processes[-1].start()
        # Remove and join any finished processes
        dead_processes = [p for p in Surface.convert_processeses if not p.is_alive()]
        for dp in dead_processes: 
            dp.join()
        Surface.convert_processeses = processes
    
    def send(self, to_draw):
        for row in range(len(to_draw)):
            py_output.write_row(row, to_draw[row])
        py_output.latch()
    
    def clear(self):
        self.fill_rect()
   
    def fill_rect(self, color=0, 
                  display_model = None, 
                  rows = (0,SEGMENTS_HEIGHT), 
                  columns = (0,SEGMENTS_WIDTH), 
                  fast = True):
        """
        Fills a rectangle on the board with a color or copies of a display
        """
        if not display_model: display_model = LCD_display(color = color)
        for row in range(*rows):
            for col in range(*columns):
                if fast: 
                    self.segments[row][col] = display_model
                    self.writable[row][col] = False
                else: 
                    self.segments[row][col] = LCD_display(display_model)
                    self.writable[row][col] = True
    
    def get_segment(self, row, col):
        return self.segments[row][col]
    
    def set_segment(self, row, col, segment):
        try: 
          self.segments[row][col] = segment
        except IndexError:
            raise IndexError("list index out of range at row {0} col {1} out of ".format(row, col))
    
    def get_size(self):
        return (self.width, self.height)
    
    def __getitem__(self, tup):
        row, col = tup
        segment = self.get_segment(row // SEGMENTS_HEIGHT, col // SEGMENTS_WIDTH)
        return segment[row % SEGMENTS_HEIGHT][col % SEGMENTS_WIDTH]
        
    def __setitem__(self, tup, color):
        row, col = tup
        self.put_pixel(row, col, color)
    
    def __repr__(self):
        base = ""
        return base
        
    
class LCD_display(object):
    """ Represents one LCD display"""
    HEIGHT = 16
    WIDTH = 8
    def __init__(self, pixels = None, color = 0):
        self.pixels = py_output.Lcd_bitmap()
        self.width, self.height = LCD_display.WIDTH, LCD_display.HEIGHT 
        if pixels:
            for i in range(len(self.pixels)):
                self.pixels[i] = 1 if pixels[i] else 0
                    
        elif color:
            #[True]*LCD_display.HEIGHT*LCD_display.WIDTH
            for i in range(len(self.pixels)):
                self.pixels[i] = 1
    
    def __getitem__(self, pos):
        return self.pixels[pos]
    
    def __getstate__(self):
        return {'pixels':list(self.pixels), 
                'width':self.width,
                'height':self.height}
                
    def __setstate__(self, state):
        """Used when the object should be serialized (ie picled) 
        Probably implemented as a failed attempt to get the C_types
        to be serializable and sent to another process (ie rendering).
        Might even slow things down now.
        """
        self.pixels = py_output.Lcd_bitmap(*state['pixels'])
        self.width = state['width']
        self.height = state['height']
    
    def __setitem__(self, pos, color):
        self.pixels[pos]
    
    def __repr__(self):
        return 'X'
        # rows = ''
        # for y in range(LCD_display.HEIGHT):
        #     for x in range(LCD_display.WIDTH):
        #         rows += '*' if self.pixels[y * LCD_display.WIDTH + x] else ' '
        #     rows += '\n'
        # return rows

def __at_exit():
    """ Releases all GPIO pins when the interpreter exits """

    if DEBUG > 0: print("Waiting for conversion processes to close...")
    for p in Surface.convert_processeses:
        p.join()
    if DEBUG > 0: print("Conversion processes closed, closing rendering...")
    try:
        render_queue.put(None, timeout=0.5)
    except queue.Full:
        print("RENDER QUEUE FULL AT SHUTTDOWN")
        pass
    if DEBUG > 0: print("Waiting for render processes to close...")
    render_process.join(0.5)
    if DEBUG > 0: print('Render process closed, closing IO channels...')
    py_output.cleanup()
    if DEBUG > 0: print('IO channels closed, exiting.')

atexit.register(__at_exit)
        
def main():
    # c_funcs = ctypes.CDLL('./output_map.so')
    # lcd1 = LCD_display()
    # lcd2 = LCD_display()
    # c_funcs.bmp2segment(lcd1.pixels, lcd2.pixels)
    py_output.c_funcs.myprint()
    surface = Surface()
    surface.fill_rect(color = 1)
    surface.render()
    
if __name__ == '__main__': cProfile.run("main()")

    
    
    
    
    
    
    
    
    
    
