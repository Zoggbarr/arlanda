import requests, json, time, cProfile, sys, re, datetime
from time import strftime, localtime
import pane

PLACE_KEY = '2a12884fcd21429f97684bd315dd7008'
REALTIME_KEY = 'f399f44068c941dca17e0921759c9fe4'

REQUEST_FORMAT = 'json'
DEFAULT_STATION = 'Teknis'
TRANSPORT_MODES = ["Buses", "Metros", "Trains", "Trams", "Ships"]

_station_id = None
_station_name = None

def datetime_parser(dct):
    """Convert date time stings in json to python datetime objects"""
    for k, v in dct.items():
        # 2019-07-26T17:25:00 
        re_pattern = r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}"
        date_format = "%Y-%m-%dT%H:%M:%S"
        if isinstance(v, str) and re.fullmatch(re_pattern, v):
            dct[k] = datetime.datetime.strptime(v, date_format)
    return dct

def set_station(search_string=DEFAULT_STATION):
    """Set the station from which to display departures"""
    global _station_id, _station_name
    stations_only = True
    max_results = 1
    
    request_string = "".join(
                        [f'http://api.sl.se/api2/typeahead.',
                        f'{REQUEST_FORMAT}',
                        f'?key={PLACE_KEY}',
                        f'&searchstring={search_string}',
                        f'&stationsonly={stations_only}',
                        f'&maxresults={max_results}',]
                        )
    resp_station = json.loads(
            requests.get(request_string).content,
            object_hook=datetime_parser
            )
    # print(resp_station)
    _station_id = resp_station["ResponseData"][0]["SiteId"]
    _station_name = resp_station["ResponseData"][0]["Name"]

_latest_departure_info = None

def request_api_departures(vehicles=[True,True,True,True,True], time_window=60):
    """ Request information about departures from SL API
         The station is set by calling set_station with a search string
         @param vehicles the vehicle types that should be retrieved
         @param time_window Time window where departures should be retrieved.
                            max 60
        The number of request per month is limited, do NOT call this function
        directly.
    """
    global _latest_departure_info
    if _station_id is None:
        set_station()
    if _latest_departure_info is None:
        #SL realtidsinformation, läs mer om API:et här: https://www.trafiklab.se/node/15754/documentation
        #Ange vilken kollektivtrafik man vill ska visas:
        request_string = "".join(
                            [f'http://api.sl.se/api2/realtimedeparturesV4.',
                             f'{REQUEST_FORMAT}',
                             f'?key={REALTIME_KEY}',
                             f'&siteid={_station_id}',
                             f'&timewindow={time_window}',
                             ''.join(f'&{tm}={b}' for tm, b in zip(TRANSPORT_MODES, vehicles))
                            ] )
        
        _latest_departure_info = json.loads(
            requests.get(request_string).content,
            object_hook=datetime_parser
            )
        _last_data_update = _latest_departure_info['LatestUpdate']
        _last_data_request = datetime.datetime.now()
    return _latest_departure_info

#TODO 
# Make a new request after a set time
# Make request interval dependent on time to next departure
# Sort departure by time
# Automatically decrease times without loading
def get_departures(
        line_numbers={},
        vehicle_types={},
        destinations={},
        direction=0,
        time_interval=(-1, -1)
    ):
    """ Return departure information for departures fullfiling criterieas

    
    """
    departure_info = request_api_departures()
    resp_data = departure_info["ResponseData"]
    departures = [resp_data[v] for v in vehicle_types]
    departures = [d for dep_sublist in departures for d in dep_sublist]
    filtered = []
    for d in departures:
        if ( (d['LineNumber'] in line_numbers or not line_numbers)
                and (d['Destination'] in destinations or not destinations)
                and (d['JourneyDirection'] == direction or not direction)
                ):
            filtered.append(d)
    # TODO Implement time filtering after sorting
    key_fnc = lambda d: d['ExpectedDateTime']
    filtered.sort(key=key_fnc)

    return filtered

class SlBox(pane.TextBox):
    def __init__(
            self, 
            dep_filter,
            text_format='{LineNumber} {Destination}: {DisplayTime}',
            join_string=' * ',
            **kwargs
            ):
        pane.TextBox.__init__(self, **kwargs)
        departures = get_departures(dep_filter)
        text = join_string.join(text_format.format(**d) for d in departures)
        self.set_text(text)
        
        