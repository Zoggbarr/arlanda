
# 
# By: Tobias Bolin, tbolin@kth.se
# Also: Soner Vergon

import time
import firebase, firebase_admin
import pane

class FirebaseBox(pane.TextBox):
    """
    Display text from a firebase database
    """

    def __init__(self, 
            fb_get='/screen/simpleText',
            fb_addres='https://arn-t-f8898.firebaseio.com/',
            update_interval=0,
            **kwargs):
        """ 
            fb_get: Subfolder to check for updates 
            fb_addres: Adress to firebase database
            update_interval: How often to check for updates (seconds), 
                0 means only check at startup. 

        """
        pane.TextBox.__init__(self, **kwargs)
        self.fb_get = fb_get
        self.fb_addres = fb_addres
        self._update_interval = update_interval
        self.update_interval = update_interval
        self.database = firebase.FirebaseApplication(fb_addres, None)
        self.update_content()
    
    @property
    def update_interval(self):
        if self._update_interval > 0:
            return self._update_interval
        else:
            return float('inf')

    def update_content(self):
        text = self.database.get(self.fb_get, None)
        self.last_update = time.time()
        self.set_text(text)

    def draw(self):
        if time.time() - self.last_update > self._update_interval:
            self.update_content()
        pane.TextBox.draw(self)
    